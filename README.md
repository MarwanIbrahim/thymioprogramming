# Simple Programming with Thymio robots

This repository contains simple instructions to get started programming with Thymio robots. A few examples are provided
in
[Aseba](https://www.thymio.org/products/programming-with-thymio-suite/program-thymio-aseba/), the native script language
used by Thymio robots.

## Installation

1) Clone this repository.

```bash
git clone git@gitlab.com:MarwanIbrahim/thymioprogramming.git
```

2) Install thymio suite following the [given instructions](https://www.thymio.org/download-thymio-suite/). On ubuntu
   this becomes:
    1) Install flatpak: ```sudo apt install flatpak```
    2) Install the flatpakref file for Thymio suite from
       flathub [here](https://flathub.org/apps/details/org.mobsya.ThymioSuite)
    3) Allow Thymio suite access by overriding the udev rules
   ```bash 
   cd /etc/udev/rules.d/ 
   sudo touch 99-thymio.rules 
   sudo sh -c 'echo "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"0617\", ATTRS{idProduct}==\"000a\", MODE=\"0666\" 
   SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"0617\", ATTRS{idProduct}==\"000c\", MODE=\"0666\"">>99-thymio.rules'
   ```
    4) Install the flatpak application: ```flatpak install org.mobsya.ThymioSuite.flatpakref```
3) Run thymio suite. Again on ubuntu this can be done via: ```flatpak run org.mobsya.ThymioSuite```

## Programming options

Different programming options are available for programming Thymio robots, including visual programming languages such
as Scratch, native script languages such as Aseba, or development in Python/ROS environments. A list of the different
options available can be seen under [this link](https://www.thymio.org/products/programming-with-thymio-suite/).

### Programming with Aseba

Aseba is the native script language used to program Thymio robots,
with [documentation](http://aseba.wikidot.com/en:asebausermanual)
and [tutorials](http://aseba.wikidot.com/en:thymiotutorielp1) offered for the language. Different script files (*.aesl)
are provided to display simple functions of the robot. For ease of use kindly use the Thymio Suite to access
Aseba files by choosing Aseba Studio from Thymio Suite.

#### Simple program

Programming on Thymio robots relies on capturing asynchronously generated events such as timer events, press button
events, etc. In Aseba, this can be captured via the ```onevent``` keyword. A simple example to turn on the top LED to
blue if the central button is pressed:

```
#Check button events
onevent buttons
if button.center==1 then
   call leds.top(0, 0, 32)
end
```

Note that Aseba offers two conditional branching methods; ```if condition then ... end```
and ```when condition do ... end```. As different events are updated at different rates, checking conditions from
generated events using if conditions will result in the condition being true for many function calls based on the
processor clock cycle. On the otherhand, using a when condition ensures that the condition is not evaluated multiple
times before a new event
is generated. Thus, to avoid repeated checking of the button pressed condition in the sample above, the program sample
should be:

```
#Check button events
onevent buttons
when button.center==1 do
   call leds.top(0, 0, 32)
end
```

In the case of this sample, there is little difference between using when conditions and if conditions. In other cases,
the behavior of the two checks may vary greatly (e.g: incrementing a variable to set the motor speed)

#### Flashing the robot

To flash a robot with the assigned name "thymio-II":

1) Connect your Thymio robot via USB to your device.
2) Launch Thymio Suite and open the Aseba programming tool.
3) Under "Tools" -> "Write the program(s) ..." -> "... inside thymio-II".
4) If successful, the event window should display "Flashing OK". Note that the flashed program is kept until the robot
   is restarted.

The above instructions can also be found under [this link](http://aseba.wikidot.com/en:thymioflash).


